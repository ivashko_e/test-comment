
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `news` (`id`, `title`, `text`, `created_at`, `updated_at`) VALUES
(1,	'News 1',	'text 1 text 1 text 1 text 1 text 1 text 1 text 1 \r\ntext 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 \r\ntext 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 text 1 \r\ntext 1',	'2016-02-13 01:26:18',	'2016-02-12 23:41:12');

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `news_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `text` text CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `news_id` (`news_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `comments` (`id`, `parent_id`, `news_id`, `name`, `email`, `text`, `created_at`, `updated_at`) VALUES
(1,	0,	1,	'User',	'User@mail.ru',	'Text Text Text Text Text Text Text Text Text',	'2016-02-12 18:57:23',	'2016-02-12 23:41:53'),
(2,	0,	1,	'User 1',	'email@ukr.net',	'Text1 Text1 Text1 Text1 Text1 Text1 Text1 Text1 Text1 Text1 Text1',	'2016-02-12 20:51:34',	'2016-02-12 23:41:53'),
(3,	2,	1,	'User 2',	'email2@ukr.net',	'Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2.\r\nText 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2.\r\n\r\nText 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2 Text 2.',	'2016-02-12 21:02:11',	'2016-02-12 23:41:53'),
(4,	3,	1,	'User 3',	'email3@ukr.net',	'Text 3 Text 3 Text 3 Text 3 Text 3 Text 3 Text 3 Text 3.',	'2016-02-12 19:03:17',	'2016-02-12 23:41:53'),
(5,	0,	1,	'User 4',	'email4@ukr.net',	'Text 4 Text 4 Text 4 Text 4 Text 4 Text 4 Text 4 Text 4 Text 4 Text 4',	'2016-02-12 19:40:12',	'2016-02-12 23:41:53'),
(18,	2,	1,	'User 6',	'email2@ukr.net',	'TEXT',	'2016-02-12 20:46:56',	'2016-02-12 23:41:53'),
(19,	0,	1,	'User 7',	'email7@ukr.net',	'TextTextTextText Text Text Text\r\nText Text Text Text Text',	'2016-02-12 21:11:33',	'2016-02-12 23:41:53');
