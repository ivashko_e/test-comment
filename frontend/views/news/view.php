<?php

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="news-view">

    <?php echo $this->render('/news/item', [
        'model' => $model,
    ]); ?>

    <?php

    echo $this->render('/news/comments', [
        'model' => $model,
        'dataProvider' => $dataProvider,
        'modelComments' => $modelComments,
    ]);

    ?>

</div>
