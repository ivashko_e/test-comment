<?php

use yii\helpers\Html;

?>

<div class="col-lg-12 entry">
    <div class="title">

        <h2><?= Html::encode($this->title) ?></h2>

    </div>
    <div class="POST">
        <p><?php echo nl2br($model->text); ?>
            <br /><b><?php echo $model->created_at; ?></b></p>
    </div>
</div>
