<?php

use yii\helpers\Html;
use frontend\helpers\Text;
use app\models\CommentsSearch;

?>

    <div class="media">
        <p class="pull-right">
            <small><?php echo $model['created_at']; ?></small>
        </p>
        <div class="media-body">
            <h4 class="media-heading user_name"><?php echo $model['name']; ?></h4>
            <?php echo nl2br(Text::url($model['text'])); ?>
        </div>

        <?= Html::a('reply', '#new_comment', [
            'class' => 'btn btn-primary btn-xs pull-right',
            'onclick' => '$(\'input#parent_id\').val(\'' . $model['id'] . '\');',
        ]) ?>
    </div>

    <?php if (CommentsSearch::children($model['news_id'], $model['id']) || !empty($model['children'])) : ?>

        <?php

        $children = !empty($model['children']) ? $model['children']
            : CommentsSearch::children($model['news_id'], $model['id']);

        foreach ($children as $comment) : ?>

            <div class="children">
                <?php

                echo Yii::$app->controller->renderPartial('/news/comment', [
                    'model' => $comment,
                ]);

                ?>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>