<?php

use yii\widgets\ListView;
use yii\widgets\Pjax;
use app\models\CommentsSearch;

?>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <?php Pjax::begin(['id' => 'comments_list']); ?>
                <div class="page-header">
                    <h1>
                        <?php if (CommentsSearch::count($model['id'])): ?>
                            <small class="pull-right"><?php echo CommentsSearch::count($model['id']); ?> comments</small>
                        <?php endif; ?>
                        Comments
                    </h1>
                </div>
                <div class="comments-list">

                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemOptions' => ['class' => 'item'],
                        'layout' => "{items}\n{pager}",
                        'itemView' => function ($model, $key, $index, $widget) {

                            return Yii::$app->controller->renderPartial('/news/comment', [
                                'model' => $model,
                            ]);
                        },
                    ]); ?>

                </div>
            <?php Pjax::end(); ?>

        </div>
        <div class="col-md-12">
            <h2><a name="new_comment">New comment</a></h2>

            <?= $this->render('_form', [
                'modelComments' => $modelComments,
            ]) ?>

        </div>
    </div>
</div>
