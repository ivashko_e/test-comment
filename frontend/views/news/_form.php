<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->registerJs(
    '$(function(){
        $("#comments_list").on("pjax:end", function() {
            $("#new_comment_form").attr("action", $.pjax.options.url);
        });
    });
    $(function(){
        $("#new_comment").on("pjax:end", function() {
            $.pjax.reload({container:"#comments_list"});
        });
    });'
);

?>

<div class="comments-form">

    <?php Pjax::begin(['id' => 'new_comment']); ?>

    <?php $form = ActiveForm::begin(['options' => [
        'id' => 'new_comment_form',
        'data-pjax' => 1,
    ]]); ?>

    <?= $form->field($modelComments, 'parent_id')->hiddenInput([
        'id' => 'parent_id',
    ])->label(false) ?>

    <?= $form->field($modelComments, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelComments, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelComments, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($modelComments, 'captcha')->widget(\yii\captcha\Captcha::classname(), [
        'template' => '{image} {input}',
        'options' => [
            'label' => false,
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Create', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php Pjax::end(); ?>

</div>
