<?php

namespace frontend\helpers;

use Yii;

class Text
{
    public static function url($text)
    {
        $reg_ex = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

        $text = preg_replace($reg_ex, '<a href="$0" rel="nofollow" target="_blank">$0</a>', $text);

        return $text;
    }
}