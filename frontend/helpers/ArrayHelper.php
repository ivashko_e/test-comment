<?php

namespace frontend\helpers;

use Yii;

class ArrayHelper
{
    public static function transform2forest($rows, $idName, $pidName, $childrenName = 'children')
    {
        $children = array();
        $ids = array();
        foreach ($rows as $i => $r) {
            $row = &$rows[$i];
            $id = $row[$idName];
            $pid = $row[$pidName];
            $children[$pid][$id] = &$row;
            if (!isset($children[$id]))
                $children[$id] = array();
            $row[$childrenName] = &$children[$id];
            $ids[$row[$idName]] = true;
        }

        $forest = array();
        foreach ($rows as $i => $r) {
            $row = &$rows[$i];
            if (!isset($ids[$row[$pidName]])) {
                $forest[$row[$idName]] = &$row;
            }
        }
        return $forest;
    }
}