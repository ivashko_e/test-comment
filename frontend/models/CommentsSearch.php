<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\helpers\ArrayHelper;

/**
 * CommentsSearch represents the model behind the search form about `app\models\Comments`.
 */
class CommentsSearch extends Comments
{
    static $news_id;
    static $count;
    static $comments;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'news_id'], 'integer'],
            [['name', 'email', 'text', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Comments::find()
            ->where(['news_id' => $params['news_id']])
            ->andWhere(['parent_id' => 0])
            ->orderBy(['id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {

            return $dataProvider;
        }

        return $dataProvider;
    }

    public static function count($news_id)
    {
        if (empty(self::$news_id) || self::$news_id != $news_id) {

            self::searchAll($news_id);
        }

        return self::$count;
    }

    public static function searchAll($news_id)
    {
        if (!empty(self::$news_id)) {

            return self::$comments;
        }

        self::$news_id = $news_id;

        $res = Comments::find()
            ->where(['news_id' => self::$news_id])
            ->orderBy(['id' => SORT_ASC])
            ->asArray()
            ->all();

        if (empty($res)) {

            return false;
        }

        self::$count = count($res);

        self::$comments = ArrayHelper::transform2forest($res, 'id', 'parent_id');

        return self::$comments;
    }

    public static function children($news_id, $comment_id)
    {
        if (empty(self::$news_id) || self::$news_id != $news_id) {

            self::searchAll($news_id);
        }

        if (!empty(self::$comments[$comment_id]['children'])) {

            return self::$comments[$comment_id]['children'];
        }

        return false;
    }
}
