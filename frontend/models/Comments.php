<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comments".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $news_id
 * @property string $name
 * @property string $email
 * @property string $text
 * @property string $created_at
 * @property string $updated_at
 *
 * @property News $news
 */
class Comments extends \yii\db\ActiveRecord
{
    public $captcha;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'news_id'], 'integer'],
            [['news_id', 'name', 'email', 'text', 'captcha'], 'required'],
            [['text'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'email'], 'string', 'max' => 255],
            ['captcha', 'captcha'],
            ['parent_id', 'default', 'value' => 0],
            ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'news_id' => 'News ID',
            'name' => 'Name',
            'email' => 'Email',
            'text' => 'Text',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'captcha' => '',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }
}
