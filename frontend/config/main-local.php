<?php

$config = [
    'bootstrap' => [
        'gii',
        'debug',
    ],
    'modules' => [
        'debug' => 'yii\debug\Module',
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'oNPEqOkfsTX_rcXvH5we8hJyDWl7x8f5',
        ],
    ],

];

return $config;
